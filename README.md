# keptn

## Overview

In this repository you'll find various deployment templates for deploying with the help of keptn <https://keptn.sh>.

> keptn is an event-based control plane for continuous delivery and automated operations for cloud-native applications.

You will find the Documentation for each resource within each subsection.

## Templates

This is a listing of the templates which currently can be used

| Name | Documentation                |
| ---- | ---------------------------- |
| config   | [config/README.md](config/README.md) |
| deployment   | [deployment/README.md](deployment/README.md) |
| qualitygates   | [qualitygates/README.md](qualitygates/README.md) |

## Usage

To use one of the provided templates please follow the documentation in the subsections. Here is a generic example how to implement a template.

```
include:  
  - project: 'checkelmann/keptn-templates'
    ref: master
    file: '/{templatename}.yml'
```

## Requirements

You need to create a `keptn` folder within your project where you can add your required resources.
You will find futher information in the [config section](config/README.md).

## Configuration
