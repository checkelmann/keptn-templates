# keptn onboarding

## Overview

This template will create the needed keptn resources like project and service, onboarding the `shipyard.yaml` and uploading the junit-tests.

## Usage

This template will automatically inject into the `keptn-onboarding` stage.

```yaml
stages:
  - keptn-onboarding

include:  
  - project: 'checkelmann/keptn-templates'
    ref: master
    file: '/config/onboarding.yml'
```

A sample service and deployment could be found here:

- <https://gitlab.ert.com/checkelmann/keptn-demo>

The stage is expecting a keptn folder, which can be passed over as job-artifact, with the following files

- [shipyard.yml](https://gitlab.com/checkelmann/keptn-demo/-/blob/master/keptn/shipyard.yaml) in which the stages are defined
- [dynatrace_sli.yml](https://gitlab.com/checkelmann/keptn-demo/-/blob/master/keptn/dynatrace_sli.yml) Service Level Indicators which will be checked within the quality gates
- [dynatrace_slo.yml](https://gitlab.com/checkelmann/keptn-demo/-/blob/master/keptn/dynatrace_slo.yml) Service Level Objectives which will be used to score your build
- [basiccheck.jmx](https://gitlab.com/checkelmann/keptn-demo/-/blob/master/keptn/basiccheck.jmx) Basic jmeter test (like a keepalive)
- [load.jmx](https://gitlab.com/checkelmann/keptn-demo/-/blob/master/keptn/basiccheck.jmx) Extended Jmeter Load Test
- [gitlab.conf.yaml](https://gitlab.com/checkelmann/keptn-demo/-/blob/master/keptn/basiccheck.jmx) GitLab Service Configuration


## Templates

This is a listing of the templates which currently can be used

| Name | Tagged Releases                                   |
| ---- | ------------------------------------------------- |
| onboarding.yml   | v1.0.0 |

### Configuration

The stage is expecting the following Variabled

| Environment Variable          | Description                                                             | Default      |
| ----------------------------- | ----------------------------------------------------------------------- | ------------ |
| PROJECT_NAME                  | Your Project name like gsso                                             |              |
| SERVICE_NAME                  | Your Project name like user-service                                     |              |

### Limitations

The job template uses the following job attributes:

- image
- script
- stage
  
So if you define that locally on your job, those attributes will be overwritten and the template may not work anymore.

### Customization

n/a
